# -*- coding: UTF-8 -*-
# import email
from email.mime.text import MIMEText  # email模块下的函数MIMEText对邮件的内容的构建
from email.mime.multipart import MIMEMultipart  # email模块下的函数MIMEMultipart对带附件邮件的内容的构建
from email.header import Header  # email模块下的函数Header对邮件头部构建
import smtplib  # smtplib模块是对SMTP协议的封装，导入smtplib
import time
from bs4 import BeautifulSoup

ti = time.strftime('%Y%m%d %H:%M:%S', time.localtime())


class REmail:

    def __init__(self):
        self.email_mag1 = MIMEMultipart()
        self.email_mag1.attach(MIMEText(_text='    附件中附带测试报告，请查阅 --', _subtype='plain', _charset='utf-8'))

    def add_file(self, obj1, obj2):
        att1 = MIMEText(obj1.read(), "base64", "utf-8")
        att1["Content-Type"] = 'application/octet-stream'
        # 这里的filename可以任意写，写什么名字，邮件中显示什么名字，附件名字。
        att1.add_header("Content-Disposition", "attachment", filename=("gbk", "", obj2))
        self.email_mag1.attach(att1)
        obj1.close()

    # 调用send时，请传入收件人姓名和邮箱地址
    def send(self, emails, b1):
        self.email_mag1['From'] = Header('Tester', 'utf-8')
        # self.email_mag1['To'] = Header(name, 'utf-8')   # 邮件里不显示
        self.email_mag1['Subject'] = Header(f'测试报告{ti}--{b1}', 'utf-8')
        smtp_obj1 = smtplib.SMTP()
        smtp_obj1.connect(host='smtp.qq.com', port='587')
        smtp_obj1.login(user='997987635@qq.com', password="okjrcblquwuqbccf")
        # POP3/SMTP服务   password="okjrcblquwuqbccf"
        # IMAP/SMTP服务  password='eapzaggdjruubdee'
        sender = '997987635@qq.com'
        receiver = emails
        smtp_obj1.sendmail(sender, receiver, self.email_mag1.as_string())
        print(f'测试报告{ti}发送成功.....')


with open(r".\search.html", 'r', encoding='utf-8') as x:
    t = x.read()
    text = BeautifulSoup(t, "lxml")
    text = text.find('a', id='pills-failed-tab').text
    text = text.split(' ')
    if text[-1] == '0':
        b = True
    else:
        b = False

with open(r".\search.html", 'r', encoding='utf-8') as y:
    e = REmail()
    e.add_file(y, f"搜索平台报告{ti}.html")
    e.send(['mmtai@vip.qq.com', '997987635@qq.com'], b)


if __name__ == '__main__':
    pass
